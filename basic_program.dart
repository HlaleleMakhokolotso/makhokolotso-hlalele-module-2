/*Write a basic program that stores and then prints the following data:
- Your name,
- Your favorite app,
- City
*/
void main() {
  String myName = 'Makhokolotso';
  String favoriteApp = 'CoD';
  String city = 'Bloemfontein';
  print('My name is $myName');
  print('and  my favorite app is $favoriteApp');
  print('I am from $city');
}
