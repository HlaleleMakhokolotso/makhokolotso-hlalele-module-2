/*Create an array to store all the winning apps of the Mtn Business App 
of the yaer Awards since 2012,

A) Sort and prints Apps by name;

B) Print the winning app of 2017 and the winning app of 2018;

C) then print the total number of apps from the array*/

void main() {
  List<String> winningApps = [
    'FNB',
    'SnapScan',
    'LIVE Sport',
    'WumDrop',
    'Domestly',
    'Shyft',
    'Khula Ecosystem',
    'Naked Insurance',
    'EasyEquisity',
    'Ambani'
  ];
  winningApps.sort();
  print('Winning apps from 2012 in order by app name:');
  winningApps.forEach((element) => print(element));

  print('2017 winning apps is ${winningApps[7]}');
  print('2018 winning apps is ${winningApps[4]}');

  print('Total number of apps on the array is ${winningApps.length}');
}
